package com.softvision.polyglotchat.contacts.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.USERS_REF;

/**
 * @author Gabriel Ciurdas on 12/12/2017.
 */

public class LocationFirebaseClient {
    private DatabaseReference databaseReference;

    public static final String LOCATION_REF = "location";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";

    public void saveLocation(double latitude, double longitude) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        DatabaseReference locationReference = databaseReference.child(USERS_REF)
                .child(currentUserId).child(LOCATION_REF);

        locationReference.child(LONGITUDE).setValue(String.valueOf(longitude));
        locationReference.child(LATITUDE).setValue(String.valueOf(latitude));
    }
}
