package com.softvision.polyglotchat.contacts.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.contacts.presenter.ContactCallBack;

import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.CONTACTS_REF;
import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.USERS_REF;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.EMAIL;

/**
 * @author Gabriel Ciurdas on 09/11/2017.
 */

public class ContactFirebaseClient {

    private ContactCallBack contactCallBack;
    private DatabaseReference databaseReference;
    private boolean found;

    public void attachContactCallBack(ContactCallBack contactCallBack) {
        this.contactCallBack = contactCallBack;
    }

    public void detachContactCallBack() {
        contactCallBack = null;
    }

    public void checkIfUserExists(final String email) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(USERS_REF);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child(EMAIL).getValue().equals(email)) {
                        addUserAsContact(FirebaseAuth.getInstance().getCurrentUser().getUid(), data.getKey());
                        found = true;
                    }
                }
                if(!found) {
                    contactCallBack.onUserDoesNotExist();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    private void addUserAsContact(String currentUserId, String contactId) {
        DatabaseReference currentUserReference = databaseReference.child(currentUserId);
        currentUserReference.child(CONTACTS_REF).child(contactId).setValue(R.string.contact);
        contactCallBack.onUserFound();
    }
}
