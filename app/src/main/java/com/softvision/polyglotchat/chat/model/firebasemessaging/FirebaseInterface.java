package com.softvision.polyglotchat.chat.model.firebasemessaging;

/**
 * @author Gabriel Ciurdas on 21/11/2017.
 */

interface FirebaseInterface {
    void onMessageLoadSuccess(String lastMessage, int position);
}