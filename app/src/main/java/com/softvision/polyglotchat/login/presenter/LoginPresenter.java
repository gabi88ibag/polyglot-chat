package com.softvision.polyglotchat.login.presenter;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.softvision.polyglotchat.login.model.FirebaseLoginApi;
import com.softvision.polyglotchat.login.view.FireBaseUserHandler;
import com.softvision.polyglotchat.login.view.LoginView;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;

public class LoginPresenter {

    private FireBaseUserHandler fireBaseUserHandler = new FirebaseLoginApi();
    private UserUpdateCallBack userUpdateCallBack;

    public LoginPresenter(UserUpdateCallBack userUpdateCallBack) {
        this.userUpdateCallBack = userUpdateCallBack;
    }

    public void login(String email, String password, LoginView loginView) {
       fireBaseUserHandler.login(email, password, userUpdateCallBack, loginView);
    }

    public void login(GoogleSignInResult result, LoginView loginView) {
        fireBaseUserHandler.login(result, userUpdateCallBack, loginView);
    }

    public void attachView(UserUpdateCallBack userUpdateCallBack) {
        this.userUpdateCallBack = userUpdateCallBack;
    }

    public void detachView() {
        this.userUpdateCallBack = null;
    }

}
