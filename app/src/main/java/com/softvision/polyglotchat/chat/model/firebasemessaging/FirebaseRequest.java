package com.softvision.polyglotchat.chat.model.firebasemessaging;

import android.content.res.Resources;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.model.pojo.Message;
import com.softvision.polyglotchat.chat.presenter.ChatPresenterInterface;
import com.softvision.polyglotchat.contacts.view.MessagesInterface;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;
import com.softvision.polyglotchat.utils.SharedPreferencesUtils;

import java.util.ArrayList;
import java.util.Calendar;

import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.MESSAGES_REF;
import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.USERS_REF;

@SuppressWarnings("FieldCanBeLocal")
public class FirebaseRequest implements FirebaseInterface{
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static final String NOTIFICATION_ROOM_KEY = "chatRoomKey";
    private static final String ONLINE = "online";
    static final String NOTIFICATION_MESSAGE = "message";
    static final String NOTIFICATION_NAME = "name";
    static final String NOTIFICATION_TIME = "time";
    static final String NOTIFICATION_TO = "to";
    static final String NOTIFICATION_RECEIVED_UID = "receiverUserId";
    static final String NOTIFICATION_SENDER_UID = "senderUserId";
    private static final String MESSAGE_TEXT = "messageText";
    private static String chatRoomKey = null;
    private ArrayList<Message> messageArrayList = new ArrayList<>();
    private ChatPresenterInterface chatPresenterInterface;
    private Message message;
    private String currentUserId;
    private String receiverUserId;
    private long time = Calendar.getInstance().getTimeInMillis();
    private MessagesInterface messagesInterface;

    public FirebaseRequest(ChatPresenterInterface chatPresenterInterface, String receiverUid) {
        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        receiverUserId = receiverUid;
        this.chatPresenterInterface = chatPresenterInterface;
        chatRoomKey = generateFirebaseMessageKey(currentUserId, receiverUserId);
    }

    public FirebaseRequest(MessagesInterface messagesInterface) {
        this.messagesInterface = messagesInterface;
    }

    public void sendMessageToFirebase(final String messText) {
        message = new Message();
        final DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();

        message.setSenderUid(currentUserId);
        message.setMessageText(messText);
        message.setDate(time);
        message.setMessagesSend(0);


        databaseRef.child("messages").child(chatRoomKey).push().setValue(message, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {

                    System.out.println("Data could not be saved " + databaseError.getMessage());
                } else {
                    DatabaseReference receiverRef = databaseRef.child(USERS_REF).child(receiverUserId);
                    receiverRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String presence = dataSnapshot.child(ONLINE).getValue().toString();

                            if (!presence.equals("true")) {
                                String name = SharedPreferencesUtils.getCurrentUserName();

                                sendNotification(name, messText, time, receiverUserId, currentUserId);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

        });
    }

    private void sendNotification(final String name,
                                  final String message,
                                  final long time,
                                  final String receiverUserId,
                                  String senderUid) {

        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference notificationsRef = databaseRef.child("notifications")
                .child(receiverUserId)
                .child(senderUid);

        notificationsRef.child(NOTIFICATION_NAME).setValue(name);
        notificationsRef.child(NOTIFICATION_MESSAGE).setValue(message);
        notificationsRef.child(NOTIFICATION_TIME).setValue(time);

    }

    public void getMessageList(String chatKey) {
        if (chatKey.isEmpty())
            chatKey = chatRoomKey;

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference ref = database.getReference("messages/" + chatKey);

        ref.limitToLast(100).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                messageArrayList.clear();
                for (DataSnapshot imageSnapshot : dataSnapshot.getChildren()) {

                    messageArrayList.add(0, imageSnapshot.getValue(Message.class));
                }
                chatPresenterInterface.onResponseFromFirebaseRequest(messageArrayList);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public String generateFirebaseMessageKey(String senderUid, String receiverUid) {
        Log.d("generateFirebaseKey", receiverUid + "---");
        Log.d("generateFirebaseKey", senderUid + "---");

        int compare = senderUid.compareToIgnoreCase(receiverUid);
        if (compare < 0) {
            chatRoomKey = receiverUid + "-" + senderUid;
        } else if (compare > 0) {
            chatRoomKey = senderUid + "-" + receiverUid;
        }

        return chatRoomKey;
    }

    public void getLastSeen(final String senderUid) {
        final String[] lastSeen = new String[1];

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users");
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String time = dataSnapshot.child(senderUid).child("online").getValue().toString();
                if (time.equals("true")) {
                    lastSeen[0] = ONLINE;
                } else {
                    long currentTime = Calendar.getInstance().getTimeInMillis();
                    long timeDiff = currentTime - Long.valueOf(time);

                    Resources resources = PolyglotChatApplication.getInstance().getResources();
                    if (timeDiff < MINUTE_MILLIS) {
                        lastSeen[0] = resources.getString(R.string.just_now);
                    } else if (timeDiff < 2 * MINUTE_MILLIS) {
                        lastSeen[0] = resources.getString(R.string.a_minute_ago);
                    } else if (timeDiff < 50 * MINUTE_MILLIS) {
                        lastSeen[0] = timeDiff / MINUTE_MILLIS + resources.getString(R.string.minutes_ago);
                    } else if (timeDiff < 90 * MINUTE_MILLIS) {
                        lastSeen[0] = resources.getString(R.string.an_hour_ago);;
                    } else if (timeDiff < 24 * HOUR_MILLIS) {
                        lastSeen[0] = timeDiff / HOUR_MILLIS + resources.getString(R.string.hours_ago);
                    } else if (timeDiff < 48 * HOUR_MILLIS) {
                        lastSeen[0] = resources.getString(R.string.yesterday);
                    } else {
                        lastSeen[0] = timeDiff / DAY_MILLIS + resources.getString(R.string.days_ago);
                    }
                }

                chatPresenterInterface.callbackLastSeen(lastSeen[0]);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                chatPresenterInterface.callbackLastSeen(databaseError.getMessage());
            }
        });

    }

    public void getLastMessageForUser(String userId, final int position) {

        String chatRoomKey = generateFirebaseMessageKey(FirebaseAuth.getInstance().getCurrentUser().getUid(), userId);

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(MESSAGES_REF)
                .child(chatRoomKey);

        databaseReference.limitToLast(1).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String lastMessage = "";

                for(DataSnapshot data: dataSnapshot.getChildren()) {
                    lastMessage = (String) data.child(MESSAGE_TEXT).getValue();
                }
                onMessageLoadSuccess(lastMessage, position);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onMessageLoadSuccess(String lastMessage, int position) {
        messagesInterface.onLastMessageLoaded(lastMessage, position);
    }
}
