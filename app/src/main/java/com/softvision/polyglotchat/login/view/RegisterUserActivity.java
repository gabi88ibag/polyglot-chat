package com.softvision.polyglotchat.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.presenter.RegisterUserPresenter;

import java.util.regex.Pattern;


/**
 * Created by gabriel.ciurdas on 05/09/2017.
 */

public class RegisterUserActivity extends AppCompatActivity implements RegisterUserView, UserUpdateCallBack {

    public static final String REGISTER_ACCOUNT = "register";
    public static final int REGISTER_ACCOUNT_REQUEST = 15;
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button registerAccountButton;
    private TextView signInLink;
    private ProgressBar registerProgress;
    private View focusView;
    private boolean cancel;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initializeVariables();
        onSignInLink();
        onRegisterAccount();
    }

    private void initializeVariables() {
        firstNameEditText = (EditText) findViewById(R.id.first_name);
        lastNameEditText = (EditText) findViewById(R.id.last_name);
        emailEditText = (EditText) findViewById(R.id.email);
        passwordEditText = (EditText) findViewById(R.id.password);
        signInLink = (TextView) findViewById(R.id.sign_in_link_textview);
        registerProgress = (ProgressBar) findViewById(R.id.register_progress);
    }

    private void onSignInLink() {
        signInLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterUserActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    private void onRegisterAccount() {
        registerAccountButton = (Button) findViewById(R.id.register_account_button);
        registerAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerProgress.setVisibility(View.VISIBLE);
                user = new User(firstNameEditText.getText().toString(),
                        lastNameEditText.getText().toString(),
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(), "", FirebaseInstanceId.getInstance().getToken());
                attemptRegister(user);
            }
        });
    }

    private void attemptRegister(User user) {
        firstNameEditText.setError(null);
        lastNameEditText.setError(null);
        emailEditText.setError(null);

        cancel = false;
        focusView = null;

        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String email = user.getEmail();

        if (TextUtils.isEmpty(firstName)) {
            firstNameEditText.setError(getString(R.string.error_field_required));
            focusView = firstNameEditText;
            cancel = true;
        } else if (!isFirstNameVaild(firstName)) {
            firstNameEditText.setError(getString(R.string.first_name_field_error));
            focusView = firstNameEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            lastNameEditText.setError(getString(R.string.error_field_required));
            focusView = lastNameEditText;
            cancel = true;
        } else if (!isLastNameValid(lastName)) {
            lastNameEditText.setError(getString(R.string.last_name_field_error));
            focusView = lastNameEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            focusView = emailEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            registerProgress.setVisibility(View.GONE);
            focusView.requestFocus();
        } else {

            checkEmailAlreadyExists(email);
        }
    }

    private void checkEmailAlreadyExists(String email) {
        RegisterUserPresenter registerUserPresenter = new RegisterUserPresenter(this);
        registerUserPresenter.checkIfEmailAlreadyRegistered(email, this);
    }

    private void checkPassword() {
        String password = user.getPassword();
        passwordEditText.setError(null);

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.error_invalid_password));
            focusView = passwordEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            registerProgress.setVisibility(View.GONE);
            focusView.requestFocus();
        } else {
            RegisterUserPresenter registerUserPresenter = new RegisterUserPresenter(this);
            registerUserPresenter.registerUser(user);
        }
    }

    private boolean isFirstNameVaild(String email) {
        return email.length() > 0;
    }

    private boolean isLastNameValid(String password) {
        return password.length() > 0;
    }

    private boolean isEmailValid(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }


    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    @Override
    public void onEmailError() {
        emailEditText.setError(getString(R.string.error_email_already_exists));
        registerProgress.setVisibility(View.GONE);
        focusView = emailEditText;
        focusView.requestFocus();
    }

    @Override
    public void onDatabaseError(String message) {
        emailEditText.setError(message);
        registerProgress.setVisibility(View.GONE);
        focusView = emailEditText;
        focusView.requestFocus();
    }

    @Override
    public void onEmailSuccess() {
        checkPassword();
    }


    @Override
    public void onUserDetailsUpdated() {
        Intent returnIntent = new Intent(getApplicationContext(), LoginActivity.class);
        returnIntent.putExtra(REGISTER_ACCOUNT, user);
        setResult(RESULT_OK, returnIntent);
        registerProgress.setVisibility(View.GONE);
        finish();
    }
}
