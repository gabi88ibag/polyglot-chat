package com.softvision.polyglotchat.contacts.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.model.pojo.Message;
import com.softvision.polyglotchat.chat.model.pojo.Notification;
import com.softvision.polyglotchat.chat.view.ChatActivity;
import com.softvision.polyglotchat.contacts.model.MyCurrentLocationService;
import com.softvision.polyglotchat.contacts.presenter.ContactsListPresenter;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.LoginActivity;
import com.softvision.polyglotchat.login.view.PolyglotChatActivity;
import com.softvision.polyglotchat.profile_details.presenter.ProfileDetailsPresenter;
import com.softvision.polyglotchat.profile_details.view.ProfileDetailsActivity;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.softvision.polyglotchat.chat.view.ChatActivity.NOTIFICATIONS_REF;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVED;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_NAME;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_TOKEN;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_UID;
import static com.softvision.polyglotchat.chat.view.ChatActivity.SENDER_UID;
import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.USERS_REF;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.CURRENT_USER;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.EMAIL;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.FIRST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.LAST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.STORED_IMAGE;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.USER;

public class ContactsListActivity extends PolyglotChatActivity implements ContactsListView,
        NavigationView.OnNavigationItemSelectedListener {

    private ProgressBar contactsListProgressBar;
    private Toolbar myToolbar;
    private DrawerLayout drawer;
    private TextView profileNameTextView, emailTextView;
    private ImageView profileImage;
    private ProgressBar profileImageProgressBar;
    private ContactsListPresenter contactsListPresenter;
    private ContactsListAdapter adapter;
    private RecyclerView contactsRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SharedPreferences sharedPreferences;
    private DatabaseReference usersDatabaseReference;
    private ArrayList<User> usersList;
    private ArrayList<Message> notifications;
    private Intent intentService;
    private MyCurrentLocationService myCurrentLocationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        sharedPreferences = this.getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
        attachContactsListView();
        updateContactsList();
        initializeVariables();
        initializeUserDetails();
        initializeActionBar();

    }


    private void attachContactsListView() {
        contactsListPresenter = new ContactsListPresenter();
        contactsListPresenter.attachView(this);
        Log.d("attachingView", "started");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("onNewIntent", "started");
    }

    @Override
    protected void onPause() {
        super.onPause();
        setUpNotifications();
    }

    @Override
    protected void onResume() {
        super.onResume();
        removePendingNotifications();
        updateContactsList();
    }

    private void removePendingNotifications() {
        NotificationManager notificationManager = (NotificationManager)
                PolyglotChatApplication.getInstance().getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    private void setUpNotifications() {
        usersDatabaseReference = FirebaseDatabase.getInstance().getReference(NOTIFICATIONS_REF);
        usersDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                notifications = new ArrayList<>();
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    contactsListPresenter.getNotifications(FirebaseAuth.getInstance().getCurrentUser().getUid());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void updateContactsList() {
        usersDatabaseReference = FirebaseDatabase.getInstance().getReference(USERS_REF);
        usersDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                usersList = new ArrayList<>();
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    contactsListPresenter.getUsersList(FirebaseAuth.getInstance().getCurrentUser().getUid());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            myCurrentLocationService = ((MyCurrentLocationService.LocalBinder) service).getMyCurrentLocationService();
        }

        public void onServiceDisconnected(ComponentName className) {
            myCurrentLocationService = null;
        }
    };


    private void initializeActionBar() {

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, myToolbar, R.string.drawer_open, R.string.drawer_close) {
                public void onDrawerClosed(View view) {
                    supportInvalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    supportInvalidateOptionsMenu();
                }
            };
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            //noinspection deprecation
            drawer.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        }
    }

    private void initializeVariables() {
        contactsListProgressBar = (ProgressBar) findViewById(R.id.contacts_list_progress_bar);
        contactsRecyclerView = (RecyclerView) findViewById(R.id.contacts_list_recyclerview);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View navHeader = navigationView.getHeaderView(0);
        profileNameTextView = (TextView) navHeader.findViewById(R.id.name);
        emailTextView = (TextView) navHeader.findViewById(R.id.website);
        profileImage = (ImageView) navHeader.findViewById(R.id.img_profile);
        profileImageProgressBar = (ProgressBar) navHeader.findViewById(R.id.small_profile_image_progress_bar);
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        contactsRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        contactsRecyclerView.setLayoutManager(layoutManager);

        adapter = new ContactsListAdapter(new ArrayList<User>(), this);
        startMyCurrentLocationService();

    }

    private void startMyCurrentLocationService() {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
            return;
        }
        intentService = new Intent(this, MyCurrentLocationService.class);
        startService(intentService);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intentService);
    }

    private void initializeUserDetails() {
        String downloadUrl = sharedPreferences.getString(STORED_IMAGE, null);
        String user = sharedPreferences.getString(USER, null);
        if (!TextUtils.isEmpty(user)) {
            profileNameTextView.setText(sharedPreferences.getString(FIRST_NAME, "") + " " +
                    sharedPreferences.getString(LAST_NAME, ""));
            emailTextView.setText(sharedPreferences.getString(EMAIL, ""));
        }

        if (!TextUtils.isEmpty(downloadUrl)) {
            loadProfilePicture(Uri.parse(downloadUrl));
        }
        ProfileDetailsPresenter profilePresenter = new ProfileDetailsPresenter(this);
        profilePresenter.checkIfUserHasPicture();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_contact:
                startActivity(new Intent(getApplicationContext(), ContactActivity.class));
                break;
        }
        return true;
    }

    @Override
    public void onLoadedUsers(ArrayList<User> users) {
        usersList = users;
        adapter.updateList(users);
        contactsRecyclerView.setAdapter(adapter);
        contactsRecyclerView.setVisibility(View.VISIBLE);
        contactsListProgressBar.setVisibility(View.GONE);
        setSwipeForRecyclerView();
    }

    @Override
    public void onNotifications(ArrayList<Notification> notifications) {

        Intent[] resultIntents = new Intent[notifications.size()];

        PendingIntent[] resultPendingIntents = new PendingIntent[notifications.size()];
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        for (int i = 0; i < notifications.size(); i++) {
            if (notifications.size() == 1) {
                resultIntents[i] = new Intent(this, ChatActivity.class);
            } else {
                resultIntents[i] = new Intent(this, ContactsListActivity.class);
            }

            resultIntents[i].putExtra(NOTIFICATIONS_REF, RECEIVED);
            resultIntents[i].putExtra(RECEIVER_UID, notifications.get(i).getSenderId());
            resultIntents[i].putExtra(RECEIVER_NAME, notifications.get(i).getName());

            resultPendingIntents[i] = PendingIntent.getActivity(this, 0, resultIntents[i], PendingIntent.FLAG_UPDATE_CURRENT);

            mBuilder.setSmallIcon(R.drawable.bubble_green)
                    .setContentTitle(notifications.get(i).getName())
                    .setContentText(notifications.get(i).getMessage())
                    .setAutoCancel(true)
                    .setContentIntent(resultPendingIntents[i]);

            mNotificationManager.notify(i, mBuilder.build());
        }
        notifications.clear();
    }

    @Override
    public void loadProfilePicture(Uri downloadUrl) {
        if (!TextUtils.isEmpty(downloadUrl.toString())) {
            profileImageProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(downloadUrl)
                    .centerInside()
                    .resize(300, 300)
                    .into(profileImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (profileImageProgressBar != null) {
                                profileImageProgressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    @Override
    public void failedRequest(VolleyError error) {

    }

    @Override
    public void onContactClick(User user) {

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(RECEIVER_UID, user.getUserId());
        intent.putExtra(SENDER_UID, FirebaseAuth.getInstance().getCurrentUser().getUid());
        intent.putExtra(RECEIVER_TOKEN, user.getToken());
        intent.putExtra(RECEIVER_NAME, user.getFirstName() + " " + user.getLastName());

        startActivity(intent);
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_my_profile:
                startActivity(new Intent(ContactsListActivity.this, ProfileDetailsActivity.class));
                drawer.closeDrawer(Gravity.LEFT);
                return true;
            case R.id.nav_sign_out:
                PolyglotChatApplication.getInstance().signOutCurrentUser();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setSwipeForRecyclerView() {

        DeleteContactOnSwipeUtil swipeHelper = new DeleteContactOnSwipeUtil(0, ItemTouchHelper.LEFT, ContactsListActivity.this) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();

                setUpDeleteContactConfirmDialog(swipedPosition);
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(contactsRecyclerView);
    }

    private void setUpDeleteContactConfirmDialog(final int swipedPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactsListActivity.this);
        builder.setMessage(R.string.delete_contact_question);
        builder.setPositiveButton(R.string.confirm_dialog_delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

                String userId = usersList.get(swipedPosition).getUserId();

                ContactsListAdapter adapter = (ContactsListAdapter) contactsRecyclerView.getAdapter();
                adapter.delete(swipedPosition);
                adapter.notifyDataSetChanged();

                contactsListPresenter.deleteContact(userId);

            }
        });
        builder.setNegativeButton(R.string.confirm_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                adapter.notifyDataSetChanged();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}