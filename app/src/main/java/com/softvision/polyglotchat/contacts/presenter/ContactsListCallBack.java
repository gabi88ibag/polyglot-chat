package com.softvision.polyglotchat.contacts.presenter;

import com.softvision.polyglotchat.chat.model.pojo.Notification;
import com.softvision.polyglotchat.login.model.pojo.User;

import java.util.ArrayList;

/**
 * @author Gabriel Ciurdas on 08/11/2017.
 */

public interface ContactsListCallBack {

    void onUsersLoaded(ArrayList<User> users);

    void onNotificationReceive(ArrayList<Notification> notifications);
}
