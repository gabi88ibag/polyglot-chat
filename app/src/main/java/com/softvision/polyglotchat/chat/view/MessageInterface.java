package com.softvision.polyglotchat.chat.view;

import com.softvision.polyglotchat.chat.model.pojo.Message;

import java.util.ArrayList;

public interface MessageInterface {

    void onMessageListLoaded(ArrayList<Message> messSend);

    void callbackLastSeen(String timestamp);
}
