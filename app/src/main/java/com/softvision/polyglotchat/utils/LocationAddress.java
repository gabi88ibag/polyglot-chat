package com.softvision.polyglotchat.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.softvision.polyglotchat.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationAddress {
    private static final String TAG = "LocationAddress";
    public static final String MY_ADDRESS_REQUEST = "myAddress";
    public static final String USER_ADDRESS_REQUEST = "userAddress";
    public static final String USER_ADDRESS_REQUEST_FAILED = "requestFailed";

    public static void getAddressFromLocation(final String target, final double latitude, final double longitude,
                                              final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(
                            latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        sb.append(address.getAddressLine(0));
                        result = sb.toString();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        if(target.equals(MY_ADDRESS_REQUEST)) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString(target, result);
                            message.setData(bundle);
                        }
                        if(target.equals(USER_ADDRESS_REQUEST)) {
                            message.what = 2;
                            Bundle bundle = new Bundle();
                            bundle.putString(target, result);
                            message.setData(bundle);
                        }
                    } else {
                        message.what = 3;
                        Bundle bundle = new Bundle();
                        result = context.getString(R.string.location_latitude) + latitude + context.getString(R.string.location_longitude) + longitude +
                                "\n" + context.getString(R.string.unable_to_get_address_error);
                        bundle.putString(target, result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
