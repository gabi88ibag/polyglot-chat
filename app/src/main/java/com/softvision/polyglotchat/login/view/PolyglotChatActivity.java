package com.softvision.polyglotchat.login.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.softvision.polyglotchat.utils.PolyglotChatApplication;

public class PolyglotChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        PolyglotChatApplication.getInstance().incrementRunningActivities();
    }

    @Override
    protected void onStop() {
        super.onStop();
        PolyglotChatApplication.getInstance().decrementRunningActivities();
    }
}
