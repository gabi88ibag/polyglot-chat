package com.softvision.polyglotchat.contacts.presenter;

/**
 * @author Gabriel Ciurdas on 09/11/2017.
 */

public interface ContactCallBack {

    void onUserFound();

    void onUserDoesNotExist();
}
