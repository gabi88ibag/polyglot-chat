package com.softvision.polyglotchat.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtils {

    private static SharedPreferences sharedPreferences;

    public static final String USER = "user";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String STORED_IMAGE = "storedImage";
    public static final String CURRENT_USER = "currentUser";

    public void deleteUserDetails() {
        sharedPreferences = PolyglotChatApplication.getInstance().getMyContext().getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(USER).apply();
        sharedPreferences.edit().remove(FIRST_NAME).apply();
        sharedPreferences.edit().remove(LAST_NAME).apply();
        sharedPreferences.edit().remove(EMAIL).apply();
        sharedPreferences.edit().remove(STORED_IMAGE).apply();
    }

    public void addPictureUri(String downloadUri) {
        sharedPreferences = PolyglotChatApplication.getInstance().getMyContext().getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(STORED_IMAGE, downloadUri).apply();
    }

    public static String getCurrentUserName() {
        sharedPreferences = PolyglotChatApplication.getInstance().getMyContext().getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
        String firstName = sharedPreferences.getString(FIRST_NAME, "");
        String lastName = sharedPreferences.getString(LAST_NAME, "");

        return firstName + " " + lastName;
    }
}
