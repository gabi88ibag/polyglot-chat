package com.softvision.polyglotchat.login.presenter;


import com.softvision.polyglotchat.login.model.FirebaseLoginApi;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.FireBaseUserHandler;
import com.softvision.polyglotchat.login.view.RegisterUserView;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;

/**
 * Created by Gabriel Ciurdas on 9/15/2017.
 */

public class RegisterUserPresenter {
    private FireBaseUserHandler fireBaseUserHandler = new FirebaseLoginApi();
    private UserUpdateCallBack userUpdateCallBack;

    public RegisterUserPresenter(UserUpdateCallBack userUpdateCallBack) {
        this.userUpdateCallBack = userUpdateCallBack;
    }

    public void checkIfEmailAlreadyRegistered(String email, RegisterUserView registerUserView) {
        fireBaseUserHandler.checkIfEmailAlreadyRegistered(email, registerUserView);
    }

    public void registerUser(final User user) {
        fireBaseUserHandler.registerUser(user, userUpdateCallBack);
    }

    public void updateUserLocallyFromFirebase(final String uid) {
        fireBaseUserHandler.updateUserLocallyFromFirebase(uid, userUpdateCallBack);
    }
}
