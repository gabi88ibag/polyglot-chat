package com.softvision.polyglotchat.chat.model.firebasemessaging;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.view.ChatActivity;
import com.softvision.polyglotchat.contacts.view.ContactsListActivity;
import com.softvision.polyglotchat.login.view.SplashScreenActivity;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;
import com.softvision.polyglotchat.utils.VolleyHelper;

import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_MESSAGE;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_NAME;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_RECEIVED_UID;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_ROOM_KEY;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_SENDER_UID;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_TIME;
import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_TO;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_NAME;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_TOKEN;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_UID;
import static com.softvision.polyglotchat.chat.view.ChatActivity.SENDER_UID;

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static final String MESSAGE = "message";
    private static FirebaseDatabase mDatabase;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.d("onMsgReceived", "started");


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //message will contain the Push Message
        if(remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String message = remoteMessage.getData().get(NOTIFICATION_MESSAGE);
            String time = remoteMessage.getData().get(NOTIFICATION_TIME);
            String keyChatRoom = remoteMessage.getData().get(NOTIFICATION_ROOM_KEY);
            String to = remoteMessage.getData().get(NOTIFICATION_TO);
            String receiveUid = remoteMessage.getData().get(NOTIFICATION_RECEIVED_UID);
            String senderUid = remoteMessage.getData().get(NOTIFICATION_SENDER_UID);
            String name = remoteMessage.getData().get(NOTIFICATION_NAME);

            if (!(ChatActivity.flagID).equals(receiveUid) && null != senderUid) {
                Log.d("onMsgReceived", senderUid);
                initNotification(name, message, time, keyChatRoom, to, receiveUid, senderUid);
            }
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    public void initNotification(final String name, final String message, final String time, final String chatRoom, final String token, final String receiveUid, final String senderUid) {

        Log.d("initNotif", senderUid);
        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userRef = mDatabase.getReference("users");
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String pictureUrl;
                if(dataSnapshot.child(senderUid).child("pictureURI").exists() && !TextUtils.isEmpty(senderUid)) {
                    pictureUrl = dataSnapshot.child(senderUid).child("pictureURI").getValue().toString();
                    getBitmapFromUrl(name, message, time, chatRoom, token, receiveUid, senderUid, pictureUrl);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //// TODO: 27/09/2017
            }
        });
    }

    private void getBitmapFromUrl(final String name, final String message, final String time, final String chatRoom, final String token, final String receiveUid, final String senderUid, String pictureUrl) {

        ImageRequest imgRequest = new ImageRequest(pictureUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        sendNotification(name, message, time, chatRoom, token, receiveUid, senderUid, response);
                    }
                }, 0, 0, ImageView.ScaleType.FIT_XY, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sendNotification(name, message, time, chatRoom, token, receiveUid, senderUid, null);

                error.printStackTrace();
            }
        });
        VolleyHelper.getInstance().addToRequestQueue(imgRequest);

    }

    public void sendNotification(final String name, final String message, final String time, final String chatRoom, final String token, final String receiveUid, final String senderUid, Bitmap picture) {
        Intent intent = new Intent(this, SplashScreenActivity.class);
        intent.putExtra(NOTIFICATION_ROOM_KEY, chatRoom);
        intent.putExtra(RECEIVER_TOKEN, token);
        intent.putExtra(RECEIVER_UID, receiveUid);
        intent.putExtra(SENDER_UID, senderUid);
        intent.putExtra(MESSAGE, message);
        intent.putExtra(RECEIVER_NAME, name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent backIntent = new Intent(this, ContactsListActivity.class);

        final PendingIntent pendingIntent = PendingIntent.getActivities(this, 12, new Intent[]{backIntent, intent}, PendingIntent.FLAG_ONE_SHOT);

        if (picture == null) {
            picture = BitmapFactory.decodeResource(PolyglotChatApplication.getInstance().getMyContext().getResources(),
                    R.drawable.ic_person_outline_dark_grey_25px);
        }
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.bubble_green)
                .setContentTitle(name)
                .setContentText(message)
                .setAutoCancel(true)
                .setLargeIcon(picture)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(12/* notification id */, notificationBuilder.build());
    }
}
