package com.softvision.polyglotchat.utils;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

public class RealPathFromUriUtils {

    public String getRealPathFromURI(Uri contentURI) {
        int column_index;
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = PolyglotChatApplication.getInstance().getMyContext().getContentResolver().query(contentURI, proj, null, null, null);
            assert cursor != null;
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
