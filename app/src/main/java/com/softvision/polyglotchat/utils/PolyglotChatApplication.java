package com.softvision.polyglotchat.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

public class PolyglotChatApplication extends MultiDexApplication {
    @SuppressLint("StaticFieldLeak")
    private static PolyglotChatApplication mInstance;
    private int foregroundActivities;

    public static synchronized PolyglotChatApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public Context getMyContext() {
        return getApplicationContext();
    }

    public void incrementRunningActivities() {
        if (foregroundActivities == 0) {
            setUserOnline();
        }
        foregroundActivities++;
    }

    public void decrementRunningActivities() {
        if (foregroundActivities == 1) {
            setUserOffline();
        }
        foregroundActivities--;
    }

    public void setUserOnline() {
        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(
                FirebaseAuth.getInstance().getCurrentUser().getUid()
        );
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    userRef.child("online").setValue("true");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setUserOffline() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    userRef.child("online").setValue(ServerValue.TIMESTAMP);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void signOutCurrentUser() {
        SharedPreferencesUtils utils = new SharedPreferencesUtils();
        utils.deleteUserDetails();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FireBaseUpdateToken.deleteTokenOnLogout(FirebaseAuth.getInstance().getCurrentUser().getUid());
        auth.signOut();
    }
}
