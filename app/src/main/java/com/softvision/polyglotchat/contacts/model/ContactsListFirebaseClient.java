package com.softvision.polyglotchat.contacts.model;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.chat.model.pojo.Notification;
import com.softvision.polyglotchat.contacts.presenter.ContactsListCallBack;
import com.softvision.polyglotchat.contacts.presenter.ContactsListPresenter;
import com.softvision.polyglotchat.login.model.pojo.User;

import java.util.ArrayList;

import static com.softvision.polyglotchat.chat.view.ChatActivity.NOTIFICATIONS_REF;
import static com.softvision.polyglotchat.login.model.FirebaseLoginApi.PICTURE_URL;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.EMAIL;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.FIRST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.LAST_NAME;

/**
 * @author Gabreil Ciurdas on 08/11/2017.
 */

public class ContactsListFirebaseClient {

    public static final String CONTACTS_REF = "contacts";
    public static final String USERS_REF = "users";
    public static final String MESSAGES_REF = "messages";
    public static final String PRESENCE_REF = "online";
    public static final String USER_TOKEN = "token";
    private ArrayList<User> users = new ArrayList<>();
    private ArrayList<String> contactsId = new ArrayList<>();
    private ArrayList<Notification> notifications = new ArrayList<>();
    private DatabaseReference databaseReference;
    private DatabaseReference notificationsReference;
    private ContactsListCallBack contactsListCallBack;

    public void attachContactsListCallBack(ContactsListPresenter contactsListCallBack) {
        this.contactsListCallBack = contactsListCallBack;
    }

    public void detachContactsListCallBack() {
        contactsListCallBack = null;
    }

    public void getUsersList(String userId) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(USERS_REF);
        final DatabaseReference contactsReference = databaseReference.child(userId).child(CONTACTS_REF);

        contactsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                contactsId.clear();

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    String contactId = postSnapshot.getKey().toString();
                    contactsId.add(contactId);
                }
                getUsersList(databaseReference, contactsId);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getUsersList(DatabaseReference databaseReference, final ArrayList<String> contacts) {

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                users.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    for(String contact: contacts) {
                        if(postSnapshot.getKey().equals(contact)) {
                            String userId = postSnapshot.getKey();
                            String firstName = postSnapshot.child(FIRST_NAME).getValue().toString();
                            String lastName = postSnapshot.child(LAST_NAME).getValue().toString();
                            String email = postSnapshot.child(EMAIL).getValue().toString();
                            String pictureURI = postSnapshot.child(PICTURE_URL).getValue().toString();
                            String presence = postSnapshot.child(PRESENCE_REF).getValue().toString();
                            String token = postSnapshot.child(USER_TOKEN).getValue().toString();

                            User user = new User();
                            user.setUserId(userId);
                            user.setFirstName(firstName);
                            user.setLastName(lastName);
                            user.setEmail(email);
                            user.setPictureURI(pictureURI);
                            user.setPresence(presence);
                            user.setToken(token);

                            users.add(user);
                        }
                    }

                }
                if (contactsListCallBack !=null) {
                    contactsListCallBack.onUsersLoaded(users);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (contactsListCallBack !=null) {
            contactsListCallBack.onUsersLoaded(users);
        }
    }

    public void getNotifications(String userId) {
        notificationsReference = FirebaseDatabase.getInstance().getReference().child(NOTIFICATIONS_REF).child(userId);

        notificationsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    String senderId = postSnapshot.getKey();

                    Notification notification = postSnapshot.getValue(Notification.class);
                    notification.setSenderId(senderId);

                    notifications.add(notification);
                }
                if(notifications.size() > 0) {
                    contactsListCallBack.onNotificationReceive(notifications);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void deleteContactFromDatabase(String userId) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference userReference = databaseReference.child(USERS_REF)
                .child(currentUserId)
                .child(CONTACTS_REF)
                .child(userId);

        userReference.removeValue();

    }
}
