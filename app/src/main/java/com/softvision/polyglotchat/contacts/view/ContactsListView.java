package com.softvision.polyglotchat.contacts.view;

import android.net.Uri;

import com.android.volley.VolleyError;
import com.softvision.polyglotchat.chat.model.pojo.Notification;
import com.softvision.polyglotchat.login.model.pojo.User;

import java.util.ArrayList;

/**
 * @author Gabriel Ciurdas on 08/11/2017.
 */

public interface ContactsListView {
    void onLoadedUsers(ArrayList<User> users);

    void onNotifications(ArrayList<Notification> notifications);

    void loadProfilePicture(Uri downloadUrl);

    void failedRequest(VolleyError error);

    void onContactClick(User user);
}
