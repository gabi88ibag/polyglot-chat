package com.softvision.polyglotchat.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkConnectionUtils {

    public static boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) PolyglotChatApplication.getInstance().getMyContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
