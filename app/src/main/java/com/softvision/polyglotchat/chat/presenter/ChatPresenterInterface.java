package com.softvision.polyglotchat.chat.presenter;

import com.softvision.polyglotchat.chat.model.pojo.Message;

import java.util.ArrayList;

public interface ChatPresenterInterface {
    void onResponseFromFirebaseRequest(ArrayList<Message> message);
    void callbackLastSeen(String timestamp);
}
