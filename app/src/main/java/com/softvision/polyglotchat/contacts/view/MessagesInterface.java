package com.softvision.polyglotchat.contacts.view;

/**
 * @author Gabriel Ciurdas on 21/11/2017.
 */

public interface MessagesInterface {

    void onLastMessageLoaded(String lastMessage, int position);

}
