package com.softvision.polyglotchat.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateFormatUtils {

    public static String chatMessageDateFormat(long messageDate) {
        String chatDateFormat;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(messageDate);
        chatDateFormat = dateFormat.format(calendar.getTime());

        return chatDateFormat;
    }

    public static String notificationDateFormat(long notifiacationDate) {

        String notifDate;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(notifiacationDate);
        notifDate = dateFormat.format(calendar.getTime());

        return notifDate;
    }

    public static String getDate(long milliSeconds) {
        String dataFormat = "dd-MM-yyyy hh:mm";
        SimpleDateFormat formatter = new SimpleDateFormat(dataFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

}
