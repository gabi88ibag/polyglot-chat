package com.softvision.polyglotchat.chat.view;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.model.pojo.Message;
import com.softvision.polyglotchat.chat.presenter.ChatPresenter;
import com.softvision.polyglotchat.login.view.PolyglotChatActivity;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;

import java.util.ArrayList;

import static com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest.NOTIFICATION_ROOM_KEY;

public class ChatActivity extends PolyglotChatActivity implements MessageInterface {

    public static String flagID = "";
    private static String receiverUid;
    private static String senderUid;
    private ChatPresenter chatPresenter;
    private MessageListAdapter messageAdapter;
    private String receiverToken;
    private String chatKeyRoom = "";
    private String receiverName;

    private Button sendMessage;
    private EditText typeMessage;
    private TextView chatLastSeen, chatNameView;

    public static final String RECEIVER_UID = "receiver_uid";
    public static final String SENDER_UID = "sender_uid";
    public static final String RECEIVER_TOKEN = "receiver_token";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String SENDER_NAME = "sender_name";
    public static final String NOTIFICATIONS_REF = "notifications";
    public static final String RECEIVED = "received";
    public static final String NAME = "name";
    public static final String TIME = "time";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        receiverUid = getIntent().getExtras().getString(RECEIVER_UID);
        senderUid = getIntent().getExtras().getString(SENDER_UID);
        receiverToken = getIntent().getExtras().getString(RECEIVER_TOKEN);
        receiverName = getIntent().getExtras().getString(RECEIVER_NAME);

        flagID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        initializeVariables();
        buttonListener();

        if (getIntent().getExtras().getString(NOTIFICATION_ROOM_KEY) != null) {
            chatKeyRoom = getIntent().getExtras().getString(NOTIFICATION_ROOM_KEY);
            chatPresenter.generateChatRoomKey(senderUid, receiverUid);

            chatPresenter.getChatMessages(chatKeyRoom);
        } else {
            chatPresenter.getChatMessages(chatKeyRoom);
        }
    }

    private void initializeVariables() {
        chatPresenter = new ChatPresenter(receiverUid);

        typeMessage = (EditText) findViewById(R.id.edittext_chatbox);
        sendMessage = (Button) findViewById(R.id.button_chatbox_send);
        chatLastSeen = (TextView) findViewById(R.id.chat_last_seen);
        chatNameView = (TextView) findViewById(R.id.chat_name);
        chatNameView.setText(receiverName);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_message_list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager.setReverseLayout(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        messageAdapter = new MessageListAdapter(new ArrayList<Message>());
        mRecyclerView.setAdapter(messageAdapter);
        setUpChatToolbar();
    }

    private void playSoundOnSend() {
        final MediaPlayer mediaPlayer = MediaPlayer.create(PolyglotChatApplication.getInstance().getMyContext(), R.raw.send_message_sound_effect);
        mediaPlayer.start();
    }

    private void buttonListener() {

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!typeMessage.getText().toString().isEmpty()) {
                    chatPresenter.messSend(typeMessage.getText().toString());
                    playSoundOnSend();
                    typeMessage.getText().clear();
                }

            }
        });
    }

    @Override
    public void onMessageListLoaded(ArrayList<Message> messList) {
        Log.d("onMessageListLoaded", messList + "");

        messageAdapter.updateList(messList);

    }

    @Override
    public void callbackLastSeen(String timestamp) {
        chatLastSeen.setText(timestamp);

    }

    @Override
    protected void onStart() {
        super.onStart();
        chatPresenter.attachChatView(this); //is listener attached to presenter before creating view?
        chatPresenter.getLastSeen(receiverUid);
        removePendingNotifications(receiverUid);
    }

    @Override
    protected void onStop() {
        super.onStop();
        chatPresenter.detachChatView();
    }

    private void setUpChatToolbar() {
        Toolbar toolbarChat = (Toolbar) findViewById(R.id.toolbar_chat);
        setSupportActionBar(toolbarChat);
        final android.support.v7.app.ActionBar actionBarChat = getSupportActionBar();

        if (actionBarChat != null) {
            actionBarChat.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_user_location, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.map_view:
                Intent mapIntent = new Intent(getApplicationContext(), UserMapActivity.class);
                mapIntent.putExtra(RECEIVER_NAME, receiverName);
                mapIntent.putExtra(RECEIVER_UID, receiverUid);
                startActivity(mapIntent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void removePendingNotifications(String receiverUid) {
        Log.d("removeNotif", "started");
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference notificationsReferenece = FirebaseDatabase.getInstance().getReference(NOTIFICATIONS_REF);
        DatabaseReference myNotificationsReference = notificationsReferenece.child(currentUserId).child(receiverUid);
        myNotificationsReference.removeValue();
    }
}