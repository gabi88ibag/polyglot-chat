package com.softvision.polyglotchat.login.view;


import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.softvision.polyglotchat.login.model.pojo.User;

public interface FireBaseUserHandler {

    void updateUserLocallyFromFirebase(final String userId, UserUpdateCallBack userUpdateCallBack);

    void login(String email, String password, UserUpdateCallBack userUpdateCallBack, LoginView loginView);

    void login(final GoogleSignInResult result, final UserUpdateCallBack userUpdateCallBack, LoginView loginView);

    void registerUser(final User user, final UserUpdateCallBack userUpdateCallBack);

    void checkIfEmailAlreadyRegistered(final String email, final RegisterUserView registerUserView);


}
