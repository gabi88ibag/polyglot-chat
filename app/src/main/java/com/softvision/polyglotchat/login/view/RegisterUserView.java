package com.softvision.polyglotchat.login.view;

/**
 * Created by Gabriel Ciurdas on 9/15/2017.
 */

public interface RegisterUserView {
    
    void onEmailError();

    void onDatabaseError(String message);

    void onEmailSuccess();
}
