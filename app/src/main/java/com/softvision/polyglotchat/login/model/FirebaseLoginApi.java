package com.softvision.polyglotchat.login.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.FireBaseUserHandler;
import com.softvision.polyglotchat.login.view.LoginActivity;
import com.softvision.polyglotchat.login.view.LoginView;
import com.softvision.polyglotchat.login.view.RegisterUserView;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;
import com.softvision.polyglotchat.utils.FireBaseUpdateToken;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.CURRENT_USER;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.EMAIL;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.FIRST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.LAST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.STORED_IMAGE;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.USER;

public class FirebaseLoginApi implements FireBaseUserHandler, UserUpdateCallBack {

    public static final String USERS_NODE = "users";
    public static final String PICTURE_URL = "pictureURI";
    private DatabaseReference rootRef;

    private void addUserToFirebaseDatabase(String userId, User user, UserUpdateCallBack userUpdateCallBack) {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference ref = mDatabase.getReference(USERS_NODE);
        ref.child(userId).setValue(user);
        FireBaseUpdateToken.updateTokenOnLogin(FirebaseAuth.getInstance().getCurrentUser().getUid(), FirebaseInstanceId.getInstance().getToken());

    }

    @Override
    public void login(String email, String password, final UserUpdateCallBack userUpdateCallBack, final LoginView loginView) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser userLoggedIn = task.getResult().getUser();
                            User newUser = createShortUserFormByFirebaseUserData(userLoggedIn);
                            signIn(newUser, mAuth, userUpdateCallBack);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            loginView.onInvalidCredentials();
                        }
                    }
                });
    }

    @Override
    public void login(GoogleSignInResult result, final UserUpdateCallBack userUpdateCallBack, final LoginView loginView) {
        final GoogleSignInAccount account = result.getSignInAccount();
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            User newUser = createLongUserFormByGoogleAccount(account);
                            signIn(newUser, mAuth, userUpdateCallBack);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure: " + task.getException().getMessage());
                            loginView.onInvalidCredentials();
                        }
                    }
                });
    }

    private void signIn(final User newUser, final FirebaseAuth mAuth, final UserUpdateCallBack userUpdateCallBack) {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference ref = mDatabase.getReference(USERS_NODE);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String userId = mAuth.getCurrentUser().getUid();
                if (dataSnapshot.hasChild(userId)) {
                    updateUserLocallyFromFirebase(userId, userUpdateCallBack);
                    FireBaseUpdateToken.updateTokenOnLogin(userId, FirebaseInstanceId.getInstance().getToken());
                } else {
                    addUserToFirebaseDatabase(userId, newUser, userUpdateCallBack);
                    updateUserLocallyFromFirebase(userId, userUpdateCallBack);
                    Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), "Your account has been successfully registered",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), databaseError.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void updateUserLocallyFromFirebase(final String userId, final UserUpdateCallBack userUpdateCallback) {
        rootRef = FirebaseDatabase.getInstance().getReference(USERS_NODE);
        DatabaseReference userRef = rootRef.child(userId);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    SharedPreferences sharedPreferences = PolyglotChatApplication.getInstance()
                            .getMyContext()
                            .getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(USER, userId)
                            .putString(EMAIL, (String) snapshot.child(EMAIL).getValue())
                            .putString(FIRST_NAME, (String) snapshot.child(FIRST_NAME).getValue())
                            .putString(LAST_NAME, (String) snapshot.child(LAST_NAME).getValue())
                            .putString(STORED_IMAGE, (String) snapshot.child(PICTURE_URL).getValue())
                            .apply();

                    userUpdateCallback.onUserDetailsUpdated();

                } else {
                    Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), "User does not exist.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PolyglotChatApplication.getInstance().getApplicationContext(), LoginActivity.class);
                    PolyglotChatApplication.getInstance().getApplicationContext().startActivity(intent);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });
    }

    public void registerUser(final User user, final UserUpdateCallBack userUpdateCallBack) {

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            User savedUser = new User(user.getFirstName(),
                                    user.getLastName(),
                                    user.getEmail(),
                                    user.getPresence(),
                                    user.getPictureURI(),
                                    user.getToken(),
                                    user.getContacts());

                            addUserToFirebaseDatabase(task.getResult().getUser().getUid(), savedUser, userUpdateCallBack);
                            updateUserLocallyFromFirebase(task.getResult().getUser().getUid(), userUpdateCallBack);
                            Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), "Your account has been successfully registered",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), task.getException().getMessage(),

                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void checkIfEmailAlreadyRegistered(final String email, final RegisterUserView registerUserView) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.fetchProvidersForEmail(email).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getProviders().size() > 0) {
                        Log.d(TAG, "This email address is already in use by another account.");
                        registerUserView.onEmailError();

                    } else {
                        registerUserView.onEmailSuccess();
                    }
                } else {
                    registerUserView.onDatabaseError(task.getException().getMessage());
                }
            }
        });
    }

    @NonNull
    private User createShortUserFormByFirebaseUserData(FirebaseUser userLoggedIn) {
        final User newUser = new User();
        if (userLoggedIn.getDisplayName() != null) {
            newUser.setFirstName(userLoggedIn.getDisplayName());
        }
        if (userLoggedIn.getEmail() != null) {
            newUser.setEmail(userLoggedIn.getEmail());
        }
        if (userLoggedIn.getPhotoUrl() != null) {
            newUser.setPictureURI(userLoggedIn.getPhotoUrl().toString());
        }
        return newUser;
    }

    @NonNull
    private User createLongUserFormByGoogleAccount(GoogleSignInAccount account) {
        User user = new User();
        if (account.getGivenName() != null) {
            user.setFirstName(account.getGivenName());
        }
        if (account.getFamilyName() != null) {
            user.setLastName(account.getFamilyName());
        }
        if (account.getEmail() != null) {
            user.setEmail(account.getEmail());
        }
        if (account.getIdToken() != null) {
            user.setPassword(account.getIdToken());
        }
        if (account.getPhotoUrl() != null) {
            user.setPictureURI(account.getPhotoUrl().toString());
        }
        user.setToken(FirebaseInstanceId.getInstance().getToken());
        return user;
    }

    @Override
    public void onUserDetailsUpdated() {

    }
}


