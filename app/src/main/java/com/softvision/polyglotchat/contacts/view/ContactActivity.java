package com.softvision.polyglotchat.contacts.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.contacts.presenter.ContactPresenter;
import com.softvision.polyglotchat.login.view.PolyglotChatActivity;

import static com.softvision.polyglotchat.login.view.RegisterUserActivity.EMAIL_PATTERN;

/**
 * @author Gabriel Ciurdas on 09/11/2017.
 */

public class ContactActivity extends PolyglotChatActivity implements ContactView {

    private EditText contactEmail;
    private TextView addContactError;
    private Button addContactButton, cancelButton;
    private Toolbar toolbar;

    private ContactPresenter contactPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        initializeVariables();

        onAddContactClick();
        onCancelClick();
    }

    private void onCancelClick() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ContactsListActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        contactPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        contactPresenter.detachView();
    }

    private void initializeVariables() {
        contactEmail = (EditText) findViewById(R.id.contact_email_edit_text);
        addContactError = (TextView) findViewById(R.id.add_contact_error_text_view);
        addContactButton = (Button) findViewById(R.id.add_contact_button);
        cancelButton = (Button) findViewById(R.id.cancel_contact_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        contactPresenter = new ContactPresenter();
        setSupportActionBar(toolbar);
        final ActionBar newContactActionBar = getSupportActionBar();
        newContactActionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void onAddContactClick() {
        addContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptAddContact();
            }
        });
    }

    private void attemptAddContact() {
        // Reset errors.
        contactEmail.setError(null);
        String email = contactEmail.getText().toString();

        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(email)) {
            contactEmail.setError(getString(R.string.error_field_required));
            focusView = contactEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            contactEmail.setError(getString(R.string.error_invalid_email));
            focusView = contactEmail;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            contactPresenter.attemptAddContact(email);
        }
    }

    private boolean isEmailValid(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    @Override
    public void onUserLoaded() {
        Toast.makeText(getApplicationContext(), R.string.contact_added_successfully, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), ContactsListActivity.class));
        finish();
    }

    @Override
    public void onInvalidUser() {
        addContactError.setText(R.string.user_does_not_exist);
    }

}