package com.softvision.polyglotchat.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.contacts.view.ContactsListActivity;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.presenter.LoginPresenter;
import com.softvision.polyglotchat.utils.NetworkConnectionUtils;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;

import static com.softvision.polyglotchat.login.view.RegisterUserActivity.EMAIL_PATTERN;

/**
 * A login screen that offers login via email and google.
 */
public class LoginActivity extends AppCompatActivity implements UserUpdateCallBack, LoginView{

    private EditText emailEditText, passwordEditText;
    private ProgressBar loginProgressBar;
    private Button emailSignInButton, googleSignInButton;
    private TextView registerLink;

    private LoginPresenter loginPresenter;
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions googleSignInOptions;

    private static final String TAG = "login";
    private static final int RC_SIGN_IN = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeVariables();

        onEmailSignIn();
        onGoogleSignIn();
        onSignUp();
    }

    private void initializeVariables() {
        loginPresenter = new LoginPresenter(this);
        emailEditText = (EditText) findViewById(R.id.email_login_edit_text);
        passwordEditText = (EditText) findViewById(R.id.password_login_edit_text);
        loginProgressBar = (ProgressBar) findViewById(R.id.login_progress);
        emailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        googleSignInButton = (Button) findViewById(R.id.google_signin_button);
        registerLink = (TextView) findViewById(R.id.register);
    }

    private void onEmailSignIn() {
        emailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkConnectionUtils.isNetworkConnected()) {
                    loginProgressBar.setVisibility(View.VISIBLE);
                    attemptLogin();
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onGoogleSignIn() {
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();
        googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(LoginActivity.this, "You got an error", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkConnectionUtils.isNetworkConnected()) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void onSignUp() {
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkConnectionUtils.isNetworkConnected()) {
                    Intent intent = new Intent(LoginActivity.this, RegisterUserActivity.class);
                    startActivityForResult(intent, RegisterUserActivity.REGISTER_ACCOUNT_REQUEST);
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void attemptLogin() {
        emailEditText.setError(null);
        passwordEditText.setError(null);

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            focusView = emailEditText;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        }

        if (cancel) {
            loginProgressBar.setVisibility(View.GONE);
            focusView.requestFocus();
        } else {
            loginPresenter.login(email, password, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RegisterUserActivity.REGISTER_ACCOUNT_REQUEST && resultCode == RESULT_OK) {
            User user = (User) data.getExtras().get(RegisterUserActivity.REGISTER_ACCOUNT);
            loginProgressBar.setVisibility(View.VISIBLE);
            loginPresenter.login(user.getEmail(), user.getPassword(), this);
        }

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                loginProgressBar.setVisibility(View.VISIBLE);
                loginPresenter.login(result, this);
            } else {
                Toast.makeText(this, "google sign in failed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "google sign in failed: " + result.getStatus().toString());
            }
        }
    }

    private boolean isEmailValid(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    @Override
    public void onUserDetailsUpdated() {
        loginProgressBar.setVisibility(View.GONE);
        startActivity(new Intent(LoginActivity.this, ContactsListActivity.class));
        finish();
    }

    @Override
    public void onInvalidCredentials() {
        loginProgressBar.setVisibility(View.GONE);
        Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(), "Incorrect email/password combination.",
                Toast.LENGTH_SHORT).show();

    }
}

