package com.softvision.polyglotchat.chat.model.pojo;

/**
 *
 * @author Gabriel Ciurdas on 17/11/2017.
 */

public class Notification {

    private String senderId;
    private String name;
    private String message;
    private long time;

    public Notification(String senderId, String name, String message, long time) {
        this.senderId = senderId;
        this.name = name;
        this.message = message;
        this.time = time;
    }

    public Notification() {

    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
