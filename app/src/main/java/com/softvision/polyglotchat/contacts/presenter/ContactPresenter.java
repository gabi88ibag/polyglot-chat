package com.softvision.polyglotchat.contacts.presenter;

import com.softvision.polyglotchat.contacts.model.ContactFirebaseClient;
import com.softvision.polyglotchat.contacts.view.ContactView;

/**
 * @author Gabriel Ciurdas on 09/11/2017.
 */

public class ContactPresenter implements ContactCallBack{

    private ContactView contactView;
    private ContactFirebaseClient firebaseClient;

    public void attachView(final ContactView contactView) {
        this.contactView = contactView;
        firebaseClient = new ContactFirebaseClient();
        firebaseClient.attachContactCallBack(this);
    }

    public void detachView() {
        this.contactView = null;
        firebaseClient.detachContactCallBack();
    }

    @Override
    public void onUserFound() {
        contactView.onUserLoaded();
    }

    @Override
    public void onUserDoesNotExist() {
        contactView.onInvalidUser();
    }

    public void attemptAddContact(String email) {
        firebaseClient.checkIfUserExists(email);
    }
}
