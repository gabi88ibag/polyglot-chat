package com.softvision.polyglotchat.utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

public class FireBaseUpdateToken {

    private static FirebaseDatabase mDatabase;

    public static void updateTokenOnLogin(String loggedUser, String token){

        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userRef = mDatabase.getReference("users");
        userRef.child(loggedUser).child("token").setValue(token);
    }
    public static void deleteTokenOnLogout(String loggedUser){
        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userRef = mDatabase.getReference("users");
        userRef.child(loggedUser).child("token").setValue("");
        userRef.child(loggedUser).child("online").setValue(ServerValue.TIMESTAMP);

    }
}
