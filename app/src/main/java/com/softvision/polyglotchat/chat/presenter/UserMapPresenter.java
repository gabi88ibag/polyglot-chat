package com.softvision.polyglotchat.chat.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.chat.view.UserMapView;

import static com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient.USERS_REF;
import static com.softvision.polyglotchat.contacts.model.LocationFirebaseClient.LATITUDE;
import static com.softvision.polyglotchat.contacts.model.LocationFirebaseClient.LOCATION_REF;
import static com.softvision.polyglotchat.contacts.model.LocationFirebaseClient.LONGITUDE;

/**
 * @author Gabriel Ciurdas on 15/12/2017.
 */

public class UserMapPresenter {
    private DatabaseReference userCurrentLocationReference;
    private UserMapView userMapView;
    private String userId;

    public UserMapPresenter(UserMapView userMapView, String userId) {
        this.userMapView = userMapView;
        this.userId = userId;
    }

    public void setLocationChangeListener() {
        userCurrentLocationReference = FirebaseDatabase.getInstance().getReference(USERS_REF)
                .child(userId).child(LOCATION_REF);
        userCurrentLocationReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String latitude = (String) dataSnapshot.child(LATITUDE).getValue();
                    String longitude = (String) dataSnapshot.child(LONGITUDE).getValue();
                    LatLng userCoordinates = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));

                    userMapView.onUserCoordinatesLoaded(userCoordinates);
                } else {
                    userMapView.onUserCoordinatesNotFound();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
