package com.softvision.polyglotchat.login.view;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.softvision.polyglotchat.contacts.view.ContactsListActivity;
import com.softvision.polyglotchat.login.model.FirebaseLoginApi;
import com.softvision.polyglotchat.utils.NetworkConnectionUtils;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;

public class SplashScreenActivity extends AppCompatActivity implements UserUpdateCallBack {
    private static final String LOG_TAG = "SplashScreenActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int success = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (success != ConnectionResult.SUCCESS) {
            googleApiAvailability.makeGooglePlayServicesAvailable(this);
        }

        Stetho.initializeWithDefaults(this);
        //ToDo: decide if we need to enable database persistence - FirebaseDatabase.getInstance().setPersistenceEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        FireBaseUserHandler fireBaseUserHandler = new FirebaseLoginApi();
        if (mAuth.getCurrentUser() != null) {
            if (NetworkConnectionUtils.isNetworkConnected()) {
                fireBaseUserHandler.updateUserLocallyFromFirebase(mAuth.getCurrentUser().getUid(), this);
            } else {
                startActivity(new Intent(this, LoginActivity.class));
                Toast.makeText(getApplicationContext(), "No internet connection.", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            removePendingNotifications();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

    }

    private void removePendingNotifications() {
        NotificationManager notificationManager = (NotificationManager)
                PolyglotChatApplication.getInstance().getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void onUserDetailsUpdated() {
        startActivity(new Intent(SplashScreenActivity.this, ContactsListActivity.class));
        finish();
    }
}
