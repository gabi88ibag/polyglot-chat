package com.softvision.polyglotchat.chat.view;


import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.model.pojo.Message;
import com.softvision.polyglotchat.utils.DateFormatUtils;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;

import java.util.ArrayList;


class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.MessageViewHolder> {

    private ArrayList<Message> messages;

    MessageListAdapter(ArrayList<Message> messages) {
        this.messages = messages;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rowView = inflater.inflate(R.layout.message_receive, parent, false);
        return new MessageViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder holder, final int position) {

        String myDate = DateFormatUtils.chatMessageDateFormat(messages.get(position).getDate());

        if (messages.get(position).getSenderUid().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {

            holder.contentWithBG.setBackgroundResource(R.drawable.bubble_green);
            holder.contentWithBG.getBackground()
                    .setColorFilter(ContextCompat.getColor(PolyglotChatApplication.getInstance().getApplicationContext(), R.color.silver),
                            PorterDuff.Mode.SRC_ATOP);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.messageBody.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.messageBody.setLayoutParams(layoutParams);
            holder.messageBody.setTextColor(ContextCompat.getColor(PolyglotChatApplication.getInstance().getApplicationContext(), R.color.black));
            holder.messageBody.setText(messages.get(position).getMessageText());

            layoutParams = (LinearLayout.LayoutParams) holder.messageTime.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.messageTime.setLayoutParams(layoutParams);
            holder.messageTime.setText(myDate);


        } else {

            holder.contentWithBG.setBackgroundResource(R.drawable.bubble_yellow);
            holder.contentWithBG.getBackground()
                    .setColorFilter(ContextCompat.getColor(PolyglotChatApplication.getInstance().getApplicationContext(), R.color.blue),
                            PorterDuff.Mode.SRC_ATOP);

            LinearLayout.LayoutParams layoutParams =
                    (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.messageBody.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.messageBody.setLayoutParams(layoutParams);
            holder.messageBody.setTextColor(ContextCompat.getColor(PolyglotChatApplication.getInstance().getApplicationContext(), R.color.white));
            holder.messageBody.setText(messages.get(position).getMessageText());

            layoutParams = (LinearLayout.LayoutParams) holder.messageTime.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.messageTime.setLayoutParams(layoutParams);
            holder.messageTime.setText(myDate);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.messageTime.getVisibility() == View.VISIBLE) {
                    holder.messageTime.setVisibility(View.INVISIBLE);
                } else {
                    holder.messageTime.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    void updateList(ArrayList<Message> messageArrayList) {
        messages = messageArrayList;
        notifyDataSetChanged();
    }

    static class MessageViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout content;
        ImageView senderImage;
        TextView name;
        TextView messageBody;
        TextView messageTime;
        View layout;
        LinearLayout contentWithBG;

        MessageViewHolder(View v) {
            super(v);
            layout = v;
            messageBody = (TextView) v.findViewById(R.id.text_message_body);
            messageTime = (TextView) v.findViewById(R.id.text_message_time);
            content = (LinearLayout) v.findViewById(R.id.content);
            contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);

        }
    }

}