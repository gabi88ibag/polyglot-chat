package com.softvision.polyglotchat.profile_details.view;

import android.net.Uri;

import com.softvision.polyglotchat.login.model.pojo.User;

public interface ProfileDetailsView {

    void updateProfile(User user);

    void setProfileImage(Uri downloadUri);
}
