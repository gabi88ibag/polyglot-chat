package com.softvision.polyglotchat.chat.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.presenter.UserMapPresenter;
import com.softvision.polyglotchat.utils.DirectionsAsyncTask;
import com.softvision.polyglotchat.utils.LocationAddress;

import java.util.List;

import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_NAME;
import static com.softvision.polyglotchat.chat.view.ChatActivity.RECEIVER_UID;
import static com.softvision.polyglotchat.utils.LocationAddress.MY_ADDRESS_REQUEST;
import static com.softvision.polyglotchat.utils.LocationAddress.USER_ADDRESS_REQUEST;
import static com.softvision.polyglotchat.utils.LocationAddress.USER_ADDRESS_REQUEST_FAILED;

public class UserMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        DirectionsInterface, UserMapView {

    private GoogleMap mMap;
    private ProgressBar mapLoadingProgressBar;
    private UserMapPresenter userMapPresenter;
    private String userId;
    private String userName;
    private LatLng userCoordinates;
    private Location mCurrentLocation;
    private GoogleApiClient googleApiClient;
    private Marker userPositionMarker;
    private Marker myPositionMarker;
    private ImageView directionsImage;
    private String myAddress;
    private String usersAddress;
    private static final float ZOOM_LEVEL = 16f;
    private static final int overview = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_map);

        if (getIntent().getExtras().getString(RECEIVER_UID) != null) {
            userId = getIntent().getExtras().getString(RECEIVER_UID);
        }
        if (getIntent().getExtras().getString(RECEIVER_NAME) != null) {
            userName = getIntent().getExtras().getString(RECEIVER_NAME);
        }

        mapLoadingProgressBar = (ProgressBar) findViewById(R.id.map_progress_bar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setDirectionsImageListener();
        setLocationChangeListener();
        setUpUserLocationToobar();
        buildGoogleApiClient();
    }

    private void setLocationChangeListener() {
        userMapPresenter = new UserMapPresenter(this, userId);
        userMapPresenter.setLocationChangeListener();
    }

    private void setDirectionsImageListener() {

        directionsImage = (ImageView) findViewById(R.id.directions_image_view);
        directionsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentLocation != null) {
                    myPositionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude())));
                    myPositionMarker.setVisible(false);
                    mapLoadingProgressBar.setVisibility(View.VISIBLE);

                    LocationAddress myLocationAddress = new LocationAddress();
                    myLocationAddress.getAddressFromLocation(MY_ADDRESS_REQUEST, mCurrentLocation.getLatitude(),
                            mCurrentLocation.getLongitude(), getApplicationContext(), new GeocoderHandler());

                    LocationAddress userLocationAddress = new LocationAddress();
                    userLocationAddress.getAddressFromLocation(USER_ADDRESS_REQUEST, userCoordinates.latitude,
                            userCoordinates.longitude, getApplicationContext(), new GeocoderHandler());
                } else {
                    Toast.makeText(getApplicationContext(), R.string.locations_service_is_disabled, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onDirectionsLoaded(DirectionsResult directionsResult) {
        mapLoadingProgressBar.setVisibility(View.GONE);
        if (directionsResult != null) {
            addPolyline(directionsResult, mMap);
            positionCamera(directionsResult.routes[overview], mMap);
            addMarkersToMap(directionsResult, mMap);
        }
    }

    @Override
    public void onUserCoordinatesLoaded(LatLng userCoordinates) {
        this.userCoordinates = userCoordinates;
        updateMap();
    }

    @Override
    public void onUserCoordinatesNotFound() {
        Toast.makeText(getApplicationContext(), userName
                + getString(R.string.user_has_not_shared_his_location), Toast.LENGTH_SHORT).show();
    }

    private class GeocoderHandler extends Handler {
        private static final int MY_ADDRESS_RESULT = 1;
        private static final int USER_ADDRESS_RESULT = 2;
        private static final int ERROR_RESULT = 3;

        @Override
        public void handleMessage(Message message) {
            Bundle bundle = message.getData();

            switch (message.what) {
                case MY_ADDRESS_RESULT:
                    myAddress = bundle.getString(MY_ADDRESS_REQUEST);
                    break;
                case USER_ADDRESS_RESULT:
                    usersAddress = bundle.getString(USER_ADDRESS_REQUEST);
                    if (myAddress != null && usersAddress != null) {
                        startDirectionsTask();
                    }
                    break;
                case ERROR_RESULT:
                    myAddress = bundle.getString(USER_ADDRESS_REQUEST_FAILED);
                    usersAddress = bundle.getString(USER_ADDRESS_REQUEST);
                    break;
                default:
                    myAddress = null;
                    usersAddress = null;
            }
        }
    }

    public void startDirectionsTask() {
        DirectionsAsyncTask directionsUtils = new DirectionsAsyncTask(this, myAddress, usersAddress, TravelMode.DRIVING);
        directionsUtils.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.normal_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybrid_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.satellite_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.terrain_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case R.id.none_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
            mMap.clear();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapLoadingProgressBar.setVisibility(View.GONE);

        if (googleMap != null) {
            mMap = googleMap;
        }
    }

    private void setUpUserLocationToobar() {
        Toolbar toolbarChat = (Toolbar) findViewById(R.id.map_toolbar);
        toolbarChat.setTitle(userName);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.ic_layers_black_24dp);
        toolbarChat.setOverflowIcon(drawable);

        setSupportActionBar(toolbarChat);
        final android.support.v7.app.ActionBar actionBarChat = getSupportActionBar();

        if (actionBarChat != null) {
            actionBarChat.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (mCurrentLocation != null) {
                Log.d("myLocation", mCurrentLocation.getLongitude() + ", " + mCurrentLocation.getLatitude());
            }
            updateMap();
        }
    }

    public void updateMap() {
        if (userCoordinates != null) {
            if (mCurrentLocation != null) {
                initCamera(mCurrentLocation);
            }
            if (userPositionMarker == null) {
                userPositionMarker = mMap.addMarker(new MarkerOptions().position(userCoordinates).title(userName));
                userPositionMarker.setVisible(false);

            }
            animateMarker(userPositionMarker, userCoordinates, false);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void initCamera(Location location) {
        CameraPosition position = CameraPosition.builder()
                .target(new LatLng(location.getLatitude(),
                        location.getLongitude()))
                .bearing(0.0f)
                .tilt(0.0f)
                .build();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(position), null);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setTrafficEnabled(true);
            mMap.setMyLocationEnabled(true);
            mMap.setBuildingsEnabled(true);
            mMap.setIndoorEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            return;
        }
    }

    private void animateMarker(final Marker marker, final LatLng toPosition,
                               final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;

        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude;

                marker.setPosition(new LatLng(lat, lng));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), ZOOM_LEVEL));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath));
    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        myPositionMarker.setTitle(results.routes[overview].legs[overview].startAddress);
        myPositionMarker.setVisible(true);

        userPositionMarker.remove();
        userPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(results.routes[overview].legs[overview].endLocation.lat, results.routes[overview].legs[overview].endLocation.lng))
                .title(results.routes[overview].legs[overview].startAddress).snippet(getEndLocationTitle(results)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        if (userPositionMarker != null && myPositionMarker != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(userPositionMarker.getPosition());
            builder.include(myPositionMarker.getPosition());
            LatLngBounds bounds = builder.build();
            int padding = 70; // offset from edges of the map in pixels
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));

        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(route.legs[overview].startLocation.lat, route.legs[overview].startLocation.lng), ZOOM_LEVEL));
        }
    }

    private String getEndLocationTitle(DirectionsResult results) {
        return getString(R.string.route_time) + results.routes[overview].legs[overview].duration.humanReadable
                + getString(R.string.route_distance) + results.routes[overview].legs[overview].distance.humanReadable;
    }

}

