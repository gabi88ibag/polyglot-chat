package com.softvision.polyglotchat.profile_details.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.contacts.view.ContactsListActivity;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.PolyglotChatActivity;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;
import com.softvision.polyglotchat.profile_details.presenter.ProfileDetailsPresenter;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;
import com.softvision.polyglotchat.utils.RealPathFromUriUtils;
import com.squareup.picasso.Picasso;

import static android.provider.Telephony.Carriers.PASSWORD;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.CURRENT_USER;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.EMAIL;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.FIRST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.LAST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.STORED_IMAGE;

public class ProfileDetailsActivity extends PolyglotChatActivity implements ProfileDetailsView, UserUpdateCallBack {


    private static final String TAG = "resetPassword";
    private static final int PICK_IMAGE_REQUEST = 101;
    private static final int MY_EXTERNAL_STORAGE_REQUEST = 1;
    private EditText firstName;
    private EditText lastName;
    private ImageView profileImage;
    private Button changePassword;
    private Toolbar profileDetailsToolbar;
    private ProgressBar imageProgressBar;
    private FirebaseUser currentUser;

    private ProfileDetailsPresenter profileDetailsPresenter;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_details);

        initializeVariables();
        initializeProfileDetails();
        onProfileImageClicked();
        onChangePasswordButton();
        onSaveProfileButton();

    }

    private void initializeVariables() {
        sharedPreferences = this.getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
        firstName = (EditText) findViewById(R.id.first_name_profile_details);
        lastName = (EditText) findViewById(R.id.last_name_profile_details);
        profileImage = (ImageView) findViewById(R.id.profile_image);
        changePassword = (Button) findViewById(R.id.change_profile_password);
        profileDetailsToolbar = (Toolbar) findViewById(R.id.profile_details_toolbar);
        imageProgressBar = (ProgressBar) findViewById(R.id.profile_image_progress_bar);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        setSupportActionBar(profileDetailsToolbar);
        profileDetailsActionBar();

        profileDetailsPresenter = new ProfileDetailsPresenter(this);
    }

    private void onChangePasswordButton() {
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                String emailAddress = sharedPreferences.getString(EMAIL, null);
                auth.sendPasswordResetEmail(emailAddress)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d(TAG, "Email sent.");
                                    Toast.makeText(ProfileDetailsActivity.this,
                                            "A change password confirmation email has been sent.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }

    private void onProfileImageClicked() {
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int permissionCheck = ContextCompat.checkSelfPermission(PolyglotChatApplication.getInstance().getMyContext(),
                        android.Manifest.permission.READ_EXTERNAL_STORAGE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED && canAskForPermisson()) {
                    ActivityCompat.requestPermissions(ProfileDetailsActivity.this,
                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_EXTERNAL_STORAGE_REQUEST);
                } else {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(intent, PICK_IMAGE_REQUEST);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_EXTERNAL_STORAGE_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(intent, PICK_IMAGE_REQUEST);
                } else {
                    Toast.makeText(PolyglotChatApplication.getInstance().getMyContext(),
                            R.string.change_external_storage_permission, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private boolean canAskForPermisson(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private String getRealPathFromURI(Uri contentURI) {
        RealPathFromUriUtils pathUtils = new RealPathFromUriUtils();
        return pathUtils.getRealPathFromURI(contentURI);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri selectedImageURI = data.getData();
            Log.e("getPath", selectedImageURI.getPath());

            String pathString = getRealPathFromURI(selectedImageURI);
            imageProgressBar.setVisibility(View.VISIBLE);
            profileDetailsPresenter.uploadProfilePicture(pathString);
        }
    }

    private void initializeProfileDetails() {
        firstName.setText(sharedPreferences.getString(FIRST_NAME, null));
        lastName.setText(sharedPreferences.getString(LAST_NAME, null));
        profileImage.setImageResource(R.drawable.ic_person_outline_dark_grey_25px);
        String downloadUrl = sharedPreferences.getString(STORED_IMAGE, null);

        if (!TextUtils.isEmpty(downloadUrl)) {
            imageProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext()).load(downloadUrl)
                    .centerInside()
                    .resize(300, 300)
                    .into(profileImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (imageProgressBar != null) {
                                imageProgressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }
    }

    private void onSaveProfileButton() {
        Button saveProfileButton = (Button) findViewById(R.id.save_profile_details_button);
        saveProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User(firstName.getText().toString(),
                        lastName.getText().toString(),
                        sharedPreferences.getString(EMAIL, null),
                        sharedPreferences.getString(PASSWORD, null));

                sharedPreferences.edit().putString(FIRST_NAME, user.getFirstName()).apply();
                sharedPreferences.edit().putString(LAST_NAME, user.getLastName()).apply();
                sharedPreferences.edit().putString(STORED_IMAGE, user.getPictureURI()).apply();
                sendUser(user);
            }
        });
    }

    public void sendUser(User user) {
        profileDetailsPresenter.updateUserDetails(user, this);
    }

    @Override
    public void updateProfile(User user) {
        Toast.makeText(getApplicationContext(), "Your profile has been updated.", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(ProfileDetailsActivity.this, ContactsListActivity.class));
    }

    @Override
    public void setProfileImage(final Uri downloadUri) {
        if (!TextUtils.isEmpty(downloadUri.toString())) {
            imageProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(getApplicationContext())
                    .load(downloadUri)
                    .centerInside()
                    .resize(300, 300)
                    .into(profileImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (imageProgressBar != null) {
                                imageProgressBar.setVisibility(View.GONE);
                            }
                            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            profileDetailsPresenter.saveUserProfilePictureUrl(userId, downloadUri);
                        }

                        @Override
                        public void onError() {
                            Log.e("error at profile image", this.toString());
                        }
                    });
        }
    }

    @Override
    public void onUserDetailsUpdated() {
        Toast.makeText(getApplicationContext(), "Your profile has been updated.", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(ProfileDetailsActivity.this, ContactsListActivity.class));
    }

    private void profileDetailsActionBar() {
        final android.support.v7.app.ActionBar profileDetailsActionBar = getSupportActionBar();
        if (profileDetailsActionBar != null) {
            profileDetailsActionBar.setDisplayHomeAsUpEnabled(true);
            profileDetailsActionBar.setTitle(R.string.profile_details);
        }
    }

}

