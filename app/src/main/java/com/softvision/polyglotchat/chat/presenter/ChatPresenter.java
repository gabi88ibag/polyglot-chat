package com.softvision.polyglotchat.chat.presenter;


import com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest;
import com.softvision.polyglotchat.chat.model.pojo.Message;
import com.softvision.polyglotchat.chat.view.MessageInterface;

import java.util.ArrayList;

public class ChatPresenter implements ChatPresenterInterface {
    private FirebaseRequest fireBaseRequest;
    private MessageInterface messageInterface;

    public ChatPresenter(String receiverUID) {
        fireBaseRequest = new FirebaseRequest(this, receiverUID);
    }

    public void attachChatView(final MessageInterface messageInterface) {
        this.messageInterface = messageInterface;
    }

    public void detachChatView() {
        this.messageInterface = null;
    }

    public void messSend(String messText) {
        fireBaseRequest.sendMessageToFirebase(messText);
    }

    @Override
    public void onResponseFromFirebaseRequest(ArrayList<Message> message) {
        if (messageInterface != null) {
            messageInterface.onMessageListLoaded(message);
        }

    }

    @Override
    public void callbackLastSeen(String timestamp) {
        if (messageInterface != null) {
            messageInterface.callbackLastSeen(timestamp);
        }
    }

    public void getChatMessages(String chatKeyRoom) {
        fireBaseRequest.getMessageList(chatKeyRoom);
    }

    public String generateChatRoomKey(String senderUid, String receiverUid) {
        return fireBaseRequest.generateFirebaseMessageKey(senderUid, receiverUid);
    }

    public void getLastSeen(String senderId) {
        fireBaseRequest.getLastSeen(senderId);
    }
}
