package com.softvision.polyglotchat.profile_details.presenter;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.UploadTask;
import com.softvision.polyglotchat.contacts.view.ContactsListView;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;
import com.softvision.polyglotchat.profile_details.model.ProfileDetailsFirebaseClient;
import com.softvision.polyglotchat.profile_details.view.ProfileDetailsView;
import com.softvision.polyglotchat.utils.SharedPreferencesUtils;

public class ProfileDetailsPresenter
        implements OnSuccessListener {
    private ProfileDetailsView profileDetailsView;
    private ContactsListView contactsListView;
    private ProfileDetailsFirebaseClient profileDetailsFirebaseClient = new ProfileDetailsFirebaseClient();


    public ProfileDetailsPresenter(ProfileDetailsView profileDetailsView) {
        this.profileDetailsView = profileDetailsView;
    }

    public ProfileDetailsPresenter(ContactsListView contactsListView) {
        this.contactsListView = contactsListView;
    }


    public void uploadProfilePicture(String uriString) {
        profileDetailsFirebaseClient.uploadProfilePictureToFirebaseStorage(uriString, this);
    }

    public void checkIfUserHasPicture() {
        profileDetailsFirebaseClient.checkIfUserHasPicture(this);
    }

    public void deleteUserDetails() {
        profileDetailsFirebaseClient.deleteUserDetails();
    }

    public void updateUserDetails(User user, UserUpdateCallBack userUpdateCallback) {
        profileDetailsFirebaseClient.updateUserDetails(user, userUpdateCallback);

    }

    public void saveUserProfilePictureUrl(String userId, Uri downloadUri) {
        profileDetailsFirebaseClient.saveProfilePictureUrlInFirebaseDB(userId, downloadUri);
    }

    @SuppressWarnings("VisibleForTests")
    @Override
    public void onSuccess(Object object) {
        if (object instanceof UploadTask.TaskSnapshot) {
            UploadTask.TaskSnapshot argument = (UploadTask.TaskSnapshot) object;
            Uri downloadUrl = argument.getDownloadUrl();
            Log.e("downloadUrl", downloadUrl.toString());
            profileDetailsView.setProfileImage(downloadUrl);
        }

        if (object instanceof Uri) {
            Uri uriPath = (Uri) object;
            contactsListView.loadProfilePicture(uriPath);
            SharedPreferencesUtils sharedPrefs = new SharedPreferencesUtils();
            sharedPrefs.addPictureUri(uriPath.toString());
        }

    }
}
