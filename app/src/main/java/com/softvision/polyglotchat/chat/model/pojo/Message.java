package com.softvision.polyglotchat.chat.model.pojo;

public class Message {
    private String messageText;
    private long date;
    private String senderUid;
    private int messageSend; // 0=unsent, 1=sent

    public Message() {
    }

    public Message(String messageText, long date, String senderUid, int messageSend) {
        this.messageText = messageText;
        this.date = date;
        this.senderUid = senderUid;
        this.messageSend = messageSend;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public int getMessageSend() {
        return messageSend;
    }

    public void setMessagesSend(int messageSend) {
        this.messageSend = messageSend;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageText='" + messageText + '\'' +
                ", date=" + date +
                ", senderUid='" + senderUid + '\'' +
                ", messageSend=" + messageSend +
                '}';
    }
}
