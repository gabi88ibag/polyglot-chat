package com.softvision.polyglotchat.chat.view;

import com.google.maps.model.DirectionsResult;

/**
 * @author Gabriel Ciurdas on 15/12/2017.
 */

public interface DirectionsInterface {
    void onDirectionsLoaded(DirectionsResult directionsResult);
}
