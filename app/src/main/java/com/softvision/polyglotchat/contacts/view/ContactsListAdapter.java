package com.softvision.polyglotchat.contacts.view;

import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.model.firebasemessaging.FirebaseRequest;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.utils.PolyglotChatApplication;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.softvision.polyglotchat.chat.view.ChatActivity.NOTIFICATIONS_REF;

/**
 * @author Gabriel Ciurdas on 07/11/2017.
 */

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.CustomViewHolder>
        implements MessagesInterface {

    private List<User> users;
    private ContactsListView contactsListView;
    private ArrayList<CustomViewHolder> holders = new ArrayList<>();

    public ContactsListAdapter(List<User> users, ContactsListView contactsListView) {
        this.users = users;
        this.contactsListView = contactsListView;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_row, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        final User user = users.get(position);
        holders.add(position, holder);

        holder.contactNameTextView.setText(user.getFirstName() + " " + user.getLastName());

        setLastMessagePreview(user, position);
        initializeUserImage(holder, user.getPictureURI());
        setPresence(holder, user.getPresence());
        setUpOnContactClick(holder, user);
    }

    private void setLastMessagePreview(final User user, final int position) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(NOTIFICATIONS_REF)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user.getUserId())) {
                    holders.get(position).contactEmailTextView.setTypeface(null, Typeface.BOLD);
                    holders.get(position).contactEmailTextView.setTextColor(PolyglotChatApplication.getInstance().getResources().getColor(R.color.black));
                } else {
                    holders.get(position).contactEmailTextView.setTypeface(null, Typeface.NORMAL);
                    holders.get(position).contactEmailTextView.setTextColor(PolyglotChatApplication.getInstance().getResources().getColor(R.color.colorAccent));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseRequest firebaseRequest = new FirebaseRequest(this);
        firebaseRequest.getLastMessageForUser(user.getUserId(), position);
    }

    private void setUpOnContactClick(CustomViewHolder holder, final User user) {
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactsListView.onContactClick(user);
            }
        });
    }

    private void setPresence(CustomViewHolder holder, String presence) {
        if (!presence.equals("true")) {
            holder.presenceIcon.setColorFilter(R.color.silver, PorterDuff.Mode.MULTIPLY);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    //ToDo: delete contact implementation
    public void delete(int position) {
        users.remove(position);
        notifyDataSetChanged();
    }

    public void updateList(List<User> users) {
        this.users = users;
        notifyDataSetChanged();

        if (users.size() > holders.size()) {
            holders.clear();
        }

        Log.d("holders", String.valueOf(holders.size()));
        Log.d("users", String.valueOf(users.size()));

    }

    @Override
    public void onLastMessageLoaded(String lastMessage, int position) {
        if (holders.size() > 0) {
            holders.get(position).contactEmailTextView.setText(lastMessage);
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView contactNameTextView;
        private TextView contactEmailTextView;
        private ImageView contactImage;
        private ImageView presenceIcon;
        private View view;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.contactNameTextView = (TextView) itemView.findViewById(R.id.contact_name);
            this.contactEmailTextView = (TextView) itemView.findViewById(R.id.contact_email);
            this.contactImage = (ImageView) itemView.findViewById(R.id.contact_avatar_image);
            this.presenceIcon = (ImageView) itemView.findViewById(R.id.contact_presence_ic);
        }
    }

    private void initializeUserImage(CustomViewHolder holder, String pictureUrl) {
        if (!TextUtils.isEmpty(pictureUrl)) {
            Picasso
                    .with(PolyglotChatApplication.getInstance().getMyContext())
                    .load(pictureUrl)
                    .centerInside()
                    .resize(100, 100)
                    .noPlaceholder()
                    .into(holder.contactImage);

        }
    }
}