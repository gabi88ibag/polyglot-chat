package com.softvision.polyglotchat.profile_details.model;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.softvision.polyglotchat.login.model.pojo.User;
import com.softvision.polyglotchat.login.view.UserUpdateCallBack;
import com.softvision.polyglotchat.utils.FireBaseUpdateToken;
import com.softvision.polyglotchat.utils.SharedPreferencesUtils;

import java.io.File;

import static com.softvision.polyglotchat.login.model.FirebaseLoginApi.PICTURE_URL;
import static com.softvision.polyglotchat.login.model.FirebaseLoginApi.USERS_NODE;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.FIRST_NAME;
import static com.softvision.polyglotchat.utils.SharedPreferencesUtils.LAST_NAME;

public class ProfileDetailsFirebaseClient {

    public void uploadProfilePictureToFirebaseStorage(String uriString, OnSuccessListener onSuccessListener) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();

        if (auth.getCurrentUser() != null) {
            StorageReference userRef = storageRef.child(auth.getCurrentUser().getUid());

            Uri file = Uri.fromFile(new File(uriString));
            StorageReference imageRef = userRef.child(auth.getCurrentUser().getUid());
            Task<UploadTask.TaskSnapshot> uploadTask = imageRef.putFile(file);

            uploadTask
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.e("onFailure", exception.getMessage());
                        }
                    })
                    .addOnSuccessListener(onSuccessListener);
        }
    }

    public void checkIfUserHasPicture(OnSuccessListener onSuccessListener) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageRef = storage.getReference();
        String userId = auth.getCurrentUser().getUid();

        storageRef
                .child(userId)
                .child(userId)
                .getDownloadUrl()
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Log.e("failure", exception.getMessage());
                            }
                        });
    }

    public void deleteUserDetails() {
        SharedPreferencesUtils utils = new SharedPreferencesUtils();
        utils.deleteUserDetails();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FireBaseUpdateToken.deleteTokenOnLogout(FirebaseAuth.getInstance().getCurrentUser().getUid());
        auth.signOut();
    }

    public void updateUserDetails(User user, UserUpdateCallBack userUpdateCallback) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        updateUserInFirebase(auth.getCurrentUser().getUid(), user);
        userUpdateCallback.onUserDetailsUpdated();

    }

    public void updateUserInFirebase(final String uid, final User user) {
        final FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference ref = mDatabase.getReference(USERS_NODE);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ref.child(uid).child(FIRST_NAME).setValue(user.getFirstName());
                ref.child(uid).child(LAST_NAME).setValue(user.getLastName());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });

    }

    public void saveProfilePictureUrlInFirebaseDB(final String userId, final Uri downloadUrl) {
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference ref = mDatabase.getReference(USERS_NODE);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ref.child(userId).child(PICTURE_URL).setValue(downloadUrl.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("error", databaseError.getMessage());
            }
        });
    }
}
