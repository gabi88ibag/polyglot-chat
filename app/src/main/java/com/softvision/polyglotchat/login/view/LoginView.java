package com.softvision.polyglotchat.login.view;

/**
 * @author Gabriel Ciurdas on 10/11/2017.
 */

public interface LoginView {

    void onInvalidCredentials();
}
