package com.softvision.polyglotchat.utils;

import android.os.AsyncTask;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;
import com.softvision.polyglotchat.R;
import com.softvision.polyglotchat.chat.view.DirectionsInterface;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author Gabriel Ciurdas on 15/12/2017.
 */

public class DirectionsAsyncTask extends AsyncTask<DirectionsResult, Void, Void> {
    private String origin;
    private String destination;
    private TravelMode travelMode;
    private DirectionsResult directionsResult;
    private DirectionsInterface directionsInterface;

    public DirectionsAsyncTask(DirectionsInterface directionsInterface, String origin, String destination, TravelMode travelMode) {
        this.directionsInterface = directionsInterface;
        this.origin = origin;
        this.destination = destination;
        this.travelMode = travelMode;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(PolyglotChatApplication.getInstance().getResources().getString(R.string.google_api_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    @Override
    protected Void doInBackground(DirectionsResult... params) {
        DateTime now = new DateTime();
        try {
            directionsResult = DirectionsApi.newRequest(getGeoContext())
                    .mode(travelMode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();

        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        directionsInterface.onDirectionsLoaded(directionsResult);
    }
}
