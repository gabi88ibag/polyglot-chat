package com.softvision.polyglotchat.contacts.presenter;


import com.softvision.polyglotchat.chat.model.pojo.Notification;
import com.softvision.polyglotchat.contacts.model.ContactsListFirebaseClient;
import com.softvision.polyglotchat.contacts.view.ContactsListView;
import com.softvision.polyglotchat.login.model.pojo.User;

import java.util.ArrayList;

/**
 * @author Gabriel Ciurdas on 08/11/2017.
 */

public class ContactsListPresenter implements ContactsListCallBack{

    private ContactsListView contactsListView;
    private ContactsListFirebaseClient firebaseClient;

    public void attachView(final ContactsListView contactsListView) {
        this.contactsListView = contactsListView;
        firebaseClient = new ContactsListFirebaseClient();
        firebaseClient.attachContactsListCallBack(this);
    }

    public void detachView() {
        this.contactsListView = null;
        firebaseClient.detachContactsListCallBack();
    }

    public void getUsersList(String userId) {
        firebaseClient.getUsersList(userId);
    }

    @Override
    public void onUsersLoaded(ArrayList<User> users) {
        contactsListView.onLoadedUsers(users);
    }

    @Override
    public void onNotificationReceive(ArrayList<Notification> notifications) {
        contactsListView.onNotifications(notifications);
    }

    public void getNotifications(String userId) {
        firebaseClient.getNotifications(userId);
    }

    public void deleteContact(String userId) {
        firebaseClient.deleteContactFromDatabase(userId);
    }
}
