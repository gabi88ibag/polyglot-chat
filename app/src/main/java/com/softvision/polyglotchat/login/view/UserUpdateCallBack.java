package com.softvision.polyglotchat.login.view;

/**
 * @author Gabriel Ciurdas on 07/11/2017.
 */

public interface UserUpdateCallBack {

    void onUserDetailsUpdated();

}
