package com.softvision.polyglotchat.chat.view;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Gabriel Ciurdas on 15/12/2017.
 */

public interface UserMapView {
    void onUserCoordinatesLoaded(LatLng userCoordinates);
    void onUserCoordinatesNotFound();
}
