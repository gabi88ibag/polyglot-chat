package com.softvision.polyglotchat.contacts.view;

/**
 * @author Gabriel Ciurdas on 09/11/2017.
 */

public interface ContactView {

    void onUserLoaded();

    void onInvalidUser();
}
